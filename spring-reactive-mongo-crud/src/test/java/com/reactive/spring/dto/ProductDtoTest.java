package com.reactive.spring.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductDtoTest {

    @Test
    void testProductDtoConstructorAndGetters() {
        // Arrange
        String id = "P001";
        String name = "Test Product";
        int quantity = 10;
        double price = 99.99;

        // Act
        ProductDto productDto = new ProductDto(id, name, quantity, price);

        // Assert
        assertEquals(id, productDto.getId());
        assertEquals(name, productDto.getName());
        assertEquals(quantity, productDto.getQuantity());
        assertEquals(price, productDto.getPrice(), 0.001); // Using delta for double comparison
    }

    @Test
    void testProductDtoDefaultConstructorAndSetters() {
        // Arrange
        ProductDto productDto = new ProductDto();

        // Act
        productDto.setId("P002");
        productDto.setName("Another Product");
        productDto.setQuantity(5);
        productDto.setPrice(49.99);

        // Assert
        assertEquals("P002", productDto.getId());
        assertEquals("Another Product", productDto.getName());
        assertEquals(5, productDto.getQuantity());
        assertEquals(49.99, productDto.getPrice(), 0.001); // Using delta for double compari
    }
}