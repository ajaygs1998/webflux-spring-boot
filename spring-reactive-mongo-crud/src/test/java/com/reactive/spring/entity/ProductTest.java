package com.reactive.spring.entity;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

class ProductTest {

    @Test
    void testProductConstructorAndGetters() {
        // Arrange
        String id = "P001";
        String name = "Test Product";
        int quantity = 10;
        double price = 99.99;

        // Act
        Product product = new Product(id, name, quantity, price);

        // Assert
        assertThat(product.getId()).isEqualTo(id);
        assertThat(product.getName()).isEqualTo(name);
        assertThat(product.getQuantity()).isEqualTo(quantity);
        assertThat(product.getPrice()).isEqualTo(price);
    }

    @Test
    void testProductDefaultConstructorAndSetters() {
        // Arrange
        Product product = new Product();

        // Act
        product.setId("P002");
        product.setName("Another Product");
        product.setQuantity(5);
        product.setPrice(49.99);

        // Assert
        assertThat(product.getId()).isEqualTo("P002");
        assertThat(product.getName()).isEqualTo("Another Product");
        assertThat(product.getQuantity()).isEqualTo(5);
        assertThat(product.getPrice()).isEqualTo(49.99);
    }

    // Add more test methods if needed...
}
