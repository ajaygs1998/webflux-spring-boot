package com.reactive.spring.controller;

import com.reactive.spring.dto.ProductDto;
import com.reactive.spring.service.ProductService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@WebFluxTest(controllers = ProductController.class)
class ProductControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ProductService productService;

    @Test
    void shouldReturnProductsInRange() {
        // Mocking the service method
        double min = 50.0;
        double max = 150.0;
        ProductDto product1 = new ProductDto("1", "Product 1", 20, 100.0);
        ProductDto product2 = new ProductDto("2", "Product 2", 10, 20.0);

        Flux<ProductDto> productsInRange = Flux.just(product1, product2);
        Mockito.when(productService.getProductRange(eq(min), eq(max))).thenReturn(productsInRange);

        // Performing the test
        webTestClient.get()
                .uri("/products/product-range?min={min}&max={max}", min, max)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ProductDto.class)
                .isEqualTo(productsInRange.collectList().block());
    }

    @Test
    void testGetProducts() {
        // Mocking the service method
        ProductDto product1 = new ProductDto("1", "Product 1", 20, 100.0);
        ProductDto product2 = new ProductDto("2", "Product 2", 10, 20.0);
        Flux<ProductDto> productList = Flux.just(product1, product2);
        Mockito.when(productService.getProducts()).thenReturn(productList);

        // Performing the test
        webTestClient.get()
                .uri("/products")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ProductDto.class)
                .isEqualTo(productList.collectList().block());
    }

    @Test
    void testGetProductById() {
        // Mocking the service method
        ProductDto product = new ProductDto("1", "Product 1", 20, 100.0);
        Mockito.when(productService.getProduct("1")).thenReturn(Mono.just(product));

        // Performing the test
        webTestClient.get()
                .uri("/products/1")
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductDto.class)
                .isEqualTo(product);
    }

    @Test
    void testSaveProduct() {
        // Mocking the service method
        ProductDto product = new ProductDto("1", "Product 1", 20, 100.0);
        Mockito.when(productService.saveProduct(any(Mono.class))).thenReturn(Mono.just(product));

        // Performing the test
        webTestClient.post()
                .uri("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductDto.class)
                .isEqualTo(product);
    }

    @Test
    void testUpdateProduct() {
        // Mocking the service method
        ProductDto updatedProduct = new ProductDto("1", "Product 1", 20, 100.0);
        Mockito.when(productService.updateProduct(any(Mono.class), eq("1"))).thenReturn(Mono.just(updatedProduct));

        // Performing the test
        webTestClient.put()
                .uri("/products/update/1")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(updatedProduct)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductDto.class)
                .isEqualTo(updatedProduct);
    }

    @Test
    void testDeleteProduct() {
        // Mocking the service method
        Mockito.when(productService.deleteProduct("1")).thenReturn(Mono.empty());

        // Performing the test
        webTestClient.delete()
                .uri("/products/delete/1")
                .exchange()
                .expectStatus().isOk();
    }
}
