package com.reactive.spring.service;

import com.reactive.spring.dto.ProductDto;
import com.reactive.spring.entity.Product;
import com.reactive.spring.repository.ProductRepository;
import com.reactive.spring.util.AppUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import org.springframework.data.domain.Range;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testGetProduct() {
        // Arrange
        String productId = "1";
        ProductDto product1 = new ProductDto("1", "Product 1", 20, 100.0);


        when(productRepository.findById(productId)).thenReturn(Mono.just(AppUtils.dtoToEntity(product1)));

        // Act
        Mono<ProductDto> result = productService.getProduct(productId);

        // Assert
        assertThat(result.block()).isEqualTo(product1);
        verify(productRepository, times(1)).findById(productId);
    }

    @Test
    void testGetProductRange() {
        // Arrange
        double min = 50.0;
        double max = 100.0;
        Range<Double> priceRange = Range.closed(min, max);
        ProductDto product1 = new ProductDto("1", "Product 1", 20, 100.0);
        ProductDto product2 = new ProductDto("2", "Product 2", 10, 20.0);

        List<ProductDto> productList = Arrays.asList(product1, product2);
        when(productRepository.findPriceBetween(priceRange)).thenReturn(Flux.fromIterable(productList));

        // Act
        Flux<ProductDto> result = productService.getProductRange(min, max);

        // Assert
        assertThat(result.collectList().block()).isEqualTo(productList);
        verify(productRepository, times(1)).findPriceBetween(priceRange);
    }

    @Test
    void testSaveProduct() {
        // Arrange
        ProductDto product1 = new ProductDto("1", "Product 1", 20, 100.0);


        when(productRepository.insert((Product) any())).thenReturn(Mono.just(AppUtils.dtoToEntity(product1)));

        // Act
        Mono<ProductDto> result = productService.saveProduct(Mono.just(product1));

        // Assert
        assertThat(result.block()).isEqualTo(product1);
        verify(productRepository, times(1)).insert((Product) any());
    }

    @Test
    void testUpdateProduct() {
        // Arrange
        String productId = "1";
        ProductDto existingProductDto = new ProductDto("1", "Product 1", 20, 100.0);
        ProductDto updatedProductDto = new ProductDto("1", "Product 2", 10, 20.0);


        when(productRepository.findById(productId)).thenReturn(Mono.just(AppUtils.dtoToEntity(existingProductDto)));
        when(productRepository.save(any())).thenReturn(Mono.just(AppUtils.dtoToEntity(updatedProductDto)));

        // Act
        Mono<ProductDto> result = productService.updateProduct(Mono.just(updatedProductDto), productId);

        // Assert
        assertThat(result.block()).isEqualTo(updatedProductDto);
        verify(productRepository, times(1)).findById(productId);
        verify(productRepository, times(1)).save(any());
    }

    @Test
    void testDeleteProduct() {
        // Arrange
        String productId = "1";
        when(productRepository.deleteById(productId)).thenReturn(Mono.empty());

        // Act
        Mono<Void> result = productService.deleteProduct(productId);

        // Assert
        assertThat(result).isNotNull();
        verify(productRepository, times(1)).deleteById(productId);
    }
}
