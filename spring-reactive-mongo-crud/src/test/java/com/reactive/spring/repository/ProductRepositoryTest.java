package com.reactive.spring.repository;

import com.reactive.spring.dto.ProductDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Range;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebFluxTest
class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @MockBean
    private ProductRepository productRepositoryMock;

    @Test
    void testFindPriceBetween() {
        // Arrange
        double min = 50.0;
        double max = 100.0;
        Range<Double> priceRange = Range.closed(min, max);
        ProductDto productDto1 = new ProductDto("1", "Product 1", 20, 100.0);
        ProductDto productDto2 = new ProductDto("2", "Product 2", 10, 20.0);

        Flux<ProductDto> expectedResult = Flux.just(productDto1, productDto2);

        // Mocking the repository method
        when(productRepositoryMock.findPriceBetween(any(Range.class))).thenReturn(expectedResult);

        // Act
        Flux<ProductDto> result = productRepositoryMock.findPriceBetween(priceRange);

        // Assert
        result.as(StepVerifier::create)
                .expectNext(productDto1, productDto2)
                .verifyComplete();
    }
}
