package com.reactive.springbootdemowebflux;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MonFluxTest {

    @Test
    public void tesMono() {
        Mono<?> monoString = Mono.just("sample-string").then(Mono.error(new RuntimeException("runtime exception"))).log();
        monoString.subscribe(System.out::println, (e) -> System.out.println(e.getMessage())); //subscribe method of publisher

    }

    @Test
    public void testFlux() {
        Flux<String> fluxString = Flux.just("----- \n", "spring", "Spring boot", "Hibernate", "microservices")
                .concatWithValues("AWS")
                .concatWith(Mono.error(new RuntimeException("runtime exception")))
                .log()
                .concatWithValues("End string");
        fluxString.subscribe(System.out::println, (e) -> System.out.println(e.getMessage())); //subscribe method of publisher
    }
}
