package com.reactive.springbootdemowebflux.handler;

import com.reactive.springbootdemowebflux.dao.CustomerDao;
import com.reactive.springbootdemowebflux.dto.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CustomerStreamHandler {


    private CustomerDao customerDao;

    @Autowired
    public CustomerStreamHandler(final CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public Mono<ServerResponse> getCustomer(ServerRequest serverRequest) {
        Flux<Customer> customersStream = customerDao.getCustomersStream();
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(customersStream, Customer.class);
    }

    public Mono<ServerResponse> findCustomerById(ServerRequest serverRequest) {
        Integer customerId = Integer.valueOf(serverRequest.pathVariable("input"));
//        customerDao.getCustomerList().filter(customer -> customer.getId()==customerId).take(1).single();
        Mono<Customer> customerMono = customerDao.getCustomerList().filter(c -> c.getId() == customerId).next();
        return ServerResponse.ok().body(customerMono, Customer.class);
    }

    public Mono<ServerResponse> saveCustomer(ServerRequest serverRequest) {
        Mono<Customer> customerMono = serverRequest.bodyToMono(Customer.class);
        Mono<String> saveResponse = customerMono.map(dto -> dto.getId() + " : " + dto.getName());
        return ServerResponse.ok().body(customerMono, Customer.class);

    }
}
