package com.reactive.springbootdemowebflux.router;

import com.reactive.springbootdemowebflux.handler.CustomerHandler;
import com.reactive.springbootdemowebflux.handler.CustomerStreamHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class RouterConfig {


    private CustomerHandler customerHandler;
    private CustomerStreamHandler customerStreamHandler;

    @Autowired
    public RouterConfig(final CustomerHandler customerHandler, final CustomerStreamHandler customerStreamHandler) {
        this.customerHandler = customerHandler;
        this.customerStreamHandler = customerStreamHandler;
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunction() {
        return RouterFunctions.route()
                .GET("/router/customer/stream", customerStreamHandler::getCustomer)
                .GET("router/customers", customerHandler::loadCustomer)
                .GET("/router/customer/{input}", customerStreamHandler::findCustomerById)
                .POST("/router/customer/save", customerStreamHandler::saveCustomer)
                .build();
    }
}
