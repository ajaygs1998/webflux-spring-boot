package com.reactive.springbootdemowebflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDemoWebfluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDemoWebfluxApplication.class, args);
	}

}
